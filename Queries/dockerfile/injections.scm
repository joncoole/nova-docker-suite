; Script tags
; TODO: Need to omit heredoc child
(
    (shell_command) @injection.content
    (#set! injection.language "shell")
)
 
 ; Heredoc injection
 (source_file
     (run_instruction
         (shell_command
             (shell_fragment
                
             ) @injection.content.start
         )
         (heredoc_block
             (heredoc_end) @injection.content.end
         )
         (#set! injection.language "shell")
         (#set! injection.combined)
     )
 )