[
	"FROM"
	"AS"
	"RUN"
	"CMD"
	"LABEL"
	"EXPOSE"
	"ENV"
	"ADD"
	"COPY"
	"ENTRYPOINT"
	"VOLUME"
	"USER"
	"WORKDIR"
	"ARG"
	"ONBUILD"
	"STOPSIGNAL"
	"HEALTHCHECK"
	"SHELL"
	"MAINTAINER"
	"CROSS_BUILD"
] @keyword

[
	":"
	"@"
	"="
] @operator

(comment) @comment

; Image Highlighting
(image_spec
	name: (image_name) @tag.attribute.name
)

(image_spec
	tag: (image_tag) @tag.attribute.value
)

(image_spec
	digest: (image_digest) @tag.attribute
)

; From Instruction - Start
(from_instruction
	as: (image_alias) @tag.framework
)

(from_instruction
	(param) @identifier.argument
)
; From Instruction - End


(arg_instruction
	name: (unquoted_string) @identifier.property
)

(arg_instruction
	default: (unquoted_string) @value
)

; Env Instruction - Start
(env_pair
	name: (unquoted_string) @identifier.property
)

(env_pair
	value: (unquoted_string) @string
)
; Env Instruction - End

; Image Formatting
(image_spec
	(image_tag
		":" @punctuation.special
	)
	(image_digest
		"@" @punctuation.special
	)
)

; Copy Instruction - Start
(copy_instruction
	(param) @identifier.argument
)
	
(copy_instruction
	(path) @string
)
; Copy Instruction - End

; Label Instruction - Start
(label_instruction
	(label_pair
		key: (unquoted_string) @string.key
	)
)
		
(label_instruction
	(label_pair
		value: (unquoted_string) @string
	)
)
; Label Instruction - End

; JSON Array and String - Start
(json_string_array
	(json_string) @string
)

(json_string_array
	[
	  "["
	  "]"
	] @bracket
)
; JSON Array and String - End

; RUN Mount Params - Start
(mount_param
	[
	  "--"
	  "="
	] @operator
)

(mount_param
	name: "mount" @tag.attribute.name
)

(mount_param
	value: (mount_param_param) @tag.attribute.value
)
; RUN Mount Params - End

[
	(path)
	(unquoted_string)
	(double_quoted_string)
	(source_file)
] @string


(expose_port) @value.number

(expansion
  [
	"$"
	"{"
	"}"
  ] @keyword.operator
) @none


(source_file
	(line_continuation) @operator
)

(
	(variable) @identifier.constant
  (#match? @identifier.constant "^[A-Z][A-Z_0-9]*$")
)
