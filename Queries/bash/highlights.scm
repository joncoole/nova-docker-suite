[
  (string)
  (raw_string)
  (heredoc_body)
  (heredoc_start)
] @string

(variable_assignment name: (variable_name) @identifier.variable)

[
  "case"
  "do"
  "done"
  "elif"
  "else"
  "esac"
  "export"
  "fi"
  "for"
  "function"
  "if"
  "in"
  "unset"
  "while"
  "then"
] @keyword

(comment) @comment

(function_definition name: (word) @function)

(file_descriptor) @number

[
  (command_substitution)
  (process_substitution)
  (expansion)
] @embedded

(simple_expansion
  "$" @keyword.operator
  (variable_name) @identifier.variable
)

(command
  name: (command_name
    (word) @identifier.function.builtin
  )
)

(command
  argument: (concatenation
    (expansion
      (variable_name) @identifier.constant
    )
  )
)

(command
  argument: (concatenation
    (word) @string
  )
)

(command
  argument: (concatenation
    [ "$" ] @keyword.operator
  )
)

(command
  argument: (concatenation
    (expansion
      [
        "${"
        "}"
      ] @keyword.operator
    )
))

(case_statement
  (case_item
    value: (word) @keyword.statement
    ")" @operator
  )
)

(command
  argument: (simple_expansion
    (variable_name) @identifier.constant
  )
)

(command
  argument: (simple_expansion
    "$" @keyword.operator
  )
)

(command
  argument: (expansion
    (variable_name) @identifier.constant
  )
)

(command
  argument: (expansion
    [
      "${"
      "}"
    ] @keyword.operator
  )
)

(command
  argument: (
    (word) @string
  )
)

(command
  argument: (number) @value.number
)

(redirected_statement
  redirect: (file_redirect
    destination: (word) @string
  )
)

(redirected_statement
  redirect: (file_redirect
    destination: (number) @value.number
  )
)

(redirected_statement
  redirect: (file_redirect
    ">&" @keyword.operator
  )
)

[
  "$"
  "&&"
  ">"
  ">>"
  "<"
  "|"
  "="
  ";"
  ";;"
] @operator

(
  (command (_) @operator)
  (#match? @operator "^-")
)
