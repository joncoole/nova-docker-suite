#!/usr/bin/osascript
on run argv
	tell application "iTerm"
		activate
		create window with default profile
		tell current window
			tell current tab
				tell current session
					write text argv
				end tell
			end tell
		end tell
	end tell
end run
