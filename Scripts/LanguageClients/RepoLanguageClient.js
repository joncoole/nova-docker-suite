const notify = require("../Utilities/Notify");
const defaults = require("defaults.json");
const { LSPManager } = require("../Utilities/LSPManager");

const LSPType = Object.freeze({
	userPath:		"User Path",
	portableBinary:	"Portable Binary",
	none:			"Disable",
	stringToType(enumString) {
		if (enumString == LSPType.userPath) {
			return LSPType.userPath;
		} else if (enumString == LSPType.portableBinary) {
			return LSPType.portableBinary;
		}
		return LSPType.none;
	}
});

exports.RepoLanguageClient = class RepoLanguageClient {
	constructor(lc_literal) {
		this.name = lc_literal.name;
		this.repo_url = lc_literal.repo_url;
		this.syntax = lc_literal.syntax;
		this.lsp_name = lc_literal.lsp_name;
		this.service_vector = lc_literal.service_vector;
		this.user_path_conf = lc_literal.user_path_conf;
		this.languageClient = null;
		this.register_listeners();
	}
	
	// TODO: Overcomplicated, break up
	async start() {
		
		if (this.isDisabled()) return;
		
		let path = await this.getPathByLSPType();
		
		if (!this.isPathValid(path)) {
			notify.HelpNudge(
				`Path to ${this.name} is invalid`,
				'Please view settings or help section for guadiance.',
				`${this.syntax}_inv_path`
			);
			return;
		}
		
		if (this.languageClient) {
			this.languageClient.stop();
			nova.subscriptions.remove(this.languageClient);
		}
		// Create the client
		var serverOptions = {
			path: path,
			args: ["--stdio"]
		};
		var clientOptions = {
			// The set of document syntaxes for which the server is valid
			syntaxes: this.syntax
		};
		try {
			var client = new LanguageClient(this.lsp_name, this.name, serverOptions, clientOptions);
		} catch(err) {
			notify.ConfigNudge("LanguageClient failed on creation", err, `${this.syntax}_cr_err`);
		}
		
		try {
			// Start the client
			client.start();
			
			// Add the client to the subscriptions to be cleaned up
			nova.subscriptions.add(client);
			this.languageClient = client;
			this.languageClient.onDidStop(this.onStop);
		}
		catch (err) {
			// If the .start() method throws, it's likely because the path to the language server is invalid
			notify.Nudge(`${this.name} failed to launch`, err, `${this.syntax}_start_err`);
		}
	}
	
	stop() {
		if (this.languageClient) {
			this.languageClient.stop();
			nova.subscriptions.remove(this.languageClient);
			this.languageClient = null;
		}
	}
	
	onStop(err) {
		if (typeof err !== 'undefined') {
			notify.Nudge(
				err,
				`${this.name} has quit unexpectedly. Please see the extension console for additional details.`,
				`${this.syntax}_bad_exit`
			);
		}
	}
	
	async reload() {
		this.stop();
		this.start();
	}
	
	isPathValid(path) {
		try {
			if (nova.fs.stat(path)) {
				return true;
			}
		} catch(err) {
			return false;
		}
		return false;
	}
	
	isDisabled() {
		return LSPType.stringToType(
			nova.config.get(this.service_vector)
		) == LSPType.none;
	}
	
	getLSPType() {
		return LSPType.stringToType(nova.config.get(this.service_vector));
	}
	
	async getPathByLSPType() {
		
		let lspType = this.getLSPType();
		
		// TODO: Breakout to own function
		if (lspType == LSPType.portableBinary) {
			let lsp_repo = new LSPManager(this.repo_url);
			
			try {
				await lsp_repo.update();
				await lsp_repo.install(this.lsp_name);
			} catch(e) {
				notify.Nudge(`Failed to install ${this.name}`, e, `${this.syntax}_failed_inst`);
			}
			
			if (lsp_repo.is_installed(this.lsp_name)) {
				return lsp_repo.install_path(this.lsp_name);
			}
		}
		
		if (lspType == LSPType.userPath) {
			return this.getUserPath();
		} 
		
		return undefined;
	}
	
	getUserPath() {
		let userPath = nova.config.get(this.user_path_conf);
		if (userPath) {
			return userPath;
		}
		return undefined;
	}

	register_listeners() {
		nova.config.onDidChange(this.service_vector, async () => {
			this.reload();
		});
	}
	
}
