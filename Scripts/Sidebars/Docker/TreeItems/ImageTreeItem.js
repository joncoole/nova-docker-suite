class ImageTreeItem extends TreeItem {
	constructor(element) {
		super(element.name);
		this.image = "icons/tag.png";
		this.contextValue = "tag";
		this.descriptiveText = element.CreatedSince;
	}
}

module.exports = { ImageTreeItem };