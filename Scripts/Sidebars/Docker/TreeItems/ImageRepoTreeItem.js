class ImageRepoTreeItem extends TreeItem {
	constructor(element) {
		super(element.name);
		this.#collapsed_state(element);
		this.image = "icons/bounding_box.png";
		this.contextValue = "image";
	}
	
	#collapsed_state(element) {
		this.collapsibleState = element.collapsed
			? element.collapsed 
			: TreeItemCollapsibleState.Collapsed;
	}
}

module.exports = { ImageRepoTreeItem };