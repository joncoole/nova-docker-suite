class ContextTreeItem extends TreeItem {
	constructor(element) {
		super(ContextTreeItem.#label(element));
		this.command = "contexts.use";
		this.contextValue = "context";
		this.set_icons_by_conf(element);
		this.description(element);
	}
	
	set_icons_by_conf(element) {
		switch (nova.config.get("docker.context.icons")) {
			case "Power Symbols":
				this.on_icon = "icons/status/live/on.png";
				this.off_icon = "icons/status/live/off.png";
				break;
			case "Aqua Radio":
				this.on_icon = "icons/status/aqua_radio/on.png";
				this.off_icon = "icons/status/aqua_radio/off.png";
				break;
			default:
				this.on_icon = "icons/status/phone_toggle/on.png";
				this.off_icon = "icons/status/phone_toggle/off.png";
				break;
		}
		this.#set_icons(element);
	}
	
	#set_icons(element) {
		if (element.current) {
			this.image = this.on_icon;
		} else {
			this.image = this.off_icon;
		}
	}
	
	description(element) {
		try {
			let descriptors = [];
			if (nova.config.get("docker.context.desctext.desc")) {
				descriptors.push(element.Description);
			}
			if (nova.config.get("docker.context.desctext.endp")) {
				descriptors.push(element.DockerEndpoint);
			}
			if (nova.config.get("docker.context.desctext.name")) {
				descriptors.push(element.Name);
			}
			this.descriptiveText = descriptors.join(" ");
		} catch(e) {
			this.descriptiveText = "";
		}
	}
	
	static #label(element) {
		switch (nova.config.get("docker.context.label")) {
			case "DockerEndpoint":
				return element.DockerEndpoint;
			default:
				return element.Name;
		}
	}
	
}

module.exports = { ContextTreeItem };