class NetworkTreeItem extends TreeItem {
	constructor(element) {
		super(element.name);
		// this.command = "contexts.use";
		this.contextValue = "network";
		this.set_icons_by_conf(element);
		this.description(element);
	}
	
	set_icons_by_conf(element) {
		this.image = "icons/network.png";
	}
	
	description(element) {
		this.descriptiveText = "";
	}
	
	static #label(element) {
		switch (nova.config.get("docker.context.label")) {
			case "DockerEndpoint":
				return element.DockerEndpoint;
			default:
				return element.Name;
		}
	}
	
}

module.exports = { NetworkTreeItem };