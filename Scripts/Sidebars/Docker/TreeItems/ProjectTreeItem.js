class ProjectTreeItem extends TreeItem {
	constructor(element) {
		super(element.name);
		this.image = "icons/boxes.png";
		this.#collapsed_state(element);
		this.#set_context(element);
	}
	
	#collapsed_state(element) {
		this.collapsibleState = element.collapsed
			? element.collapsed 
			: TreeItemCollapsibleState.Expanded;
	}
	
	#set_context(element) {
		if (element.path) {
			this.descriptiveText = element.path;
			this.contextValue = "project.compose";
		} else {
			this.contextValue = "project.individual";	
		}
	}
}

module.exports = { ProjectTreeItem };