exports.Context = class Context {
	constructor(context_json=null) {
		if (context_json) {
			this.name = this.Name = context_json.Name;
			this.uuid = this.name;
			this.current = this.Current = context_json.Current;
			this.description = this.Description = context_json.Description;
			this.docker_endpoint = this.DockerEndpoint = context_json.DockerEndpoint;
			this.error = this.Error = context_json.Error;
			this.contextValue = 'context';
			
			this.children = [];
			this.parent = null;
		}
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
}