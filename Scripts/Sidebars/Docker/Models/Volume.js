class Volume {
	constructor(volume_json=null) {
		if (volume_json) {
			this.Name = this.name = volume_json.Name;
			this.Driver = volume_json.Driver;
			this.Group = volume_json.Group;
			this.Labels = volume_json.Labels;
			this.Scope = volume_json.Scope;
			this.Links = volume_json.Links;
			this.Size = volume_json.Size;
			this.Status = volume_json.Status;
			this.Availability = volume_json.Availability;
			this.Mountpoint = volume_json.Mountpoint;
			
			this.children = [];
			this.parent = null;
		}
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
}

module.exports = { Volume }