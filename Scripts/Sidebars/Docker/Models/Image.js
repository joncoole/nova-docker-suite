class Image {
	constructor(image_json) {
		this.repo = image_json.Repository;
		this.name = this.repo;
		this.uuid = this.ID = image_json.ID;
		this.digest = image_json.Digest;
		this.tag = image_json.Tag;
		this.contextValue = 'image';
		
		this.children = [];
		this.parent = null;
	}
	
	addChild(element) {
		element.parent = this;
		this.children.push(element);
	}
}

class ImageTag {
	constructor(image_json) {
		this.uuid = this.ID = image_json.ID;
		this.name = this.Tag = image_json.Tag;
		this.CreatedSince = image_json.CreatedSince;
		this.Repository = image_json.Repository;
		this.contextValue = 'image.tag';
		
		this.children = [];
		this.parent = null;
	}
}

class RepoMap extends Map {
	constructor() {
		super();
	}
	
	add_by_image_json(image_json) {
		let image = new Image(image_json);
		let tag = new ImageTag(image_json);
		
		if (this.get(image.repo)) {
			this.get(image.repo).addChild(tag);
		} else {
			image.addChild(tag);
			this.set(image.repo, image);
		}
	}
	
	valueArray() {
		return [...this.values()];
	}
}

module.exports = { Image, RepoMap }