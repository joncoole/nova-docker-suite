const notify = require("../../../Utilities/Notify");
const { NetworkTreeItem } = require("../TreeItems/NetworkTreeItem");
const { DockerClient } = require("../interfaces/DockerClient");
const { Network } = require("../models/Network");

class NetworkController {
	constructor() {
		this.tree = new TreeView("networks", {
			dataProvider: this
		});
		
		this.register_commands();
		this.register_listeners();
	}
	
	deactivate() {
		this.dispose();
	}
	
	reload() {
		this.tree.reload();
	}
	
	dispose() {
		this.tree.dispose();
	}
	
	async getChildren(element) {
		if (!element) {
			let networks = [];
			try {
				return await DockerClient.get_networks();
			} catch(e) {
				return networks;
			}
		}
		else {
			return element.children;
		}
	}
	
	getParent(element) {
		return element.parent;
	}
	
	getTreeItem(element) {
		return new NetworkTreeItem(element);
	}
	
	register_commands() {
		nova.commands.register("networks.remove", async () => {
			let selection = this.tree.selection[0];
			
			try {
				let res = await DockerClient.remove_network(selection.name);
				if (res == undefined) {
					this.reload();
				}
			} catch(e) {
				let warning = "Unable to remove networks.";
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("networks.reload", async () => {
			console.log("Reloading Networks");
			this.reload();
			nova.commands.invoke("events.reload");
		});
		
		nova.commands.register("networks.inspect", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			selection.forEach(async (network_id) => {
				nova.workspace.openNewTextDocument({
					content: await DockerClient.inspect_network(network_id),
					syntax: "json"
				});
			});
		});
	}
	
	register_listeners() {
	}
}

module.exports = { NetworkController }
