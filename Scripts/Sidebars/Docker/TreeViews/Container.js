const notify = require("../../../Utilities/Notify");
const { ContainerTreeItem } = require("../TreeItems/ContainerTreeItem");
const { ProjectTreeItem } = require("../TreeItems/ProjectTreeItem");
const { DockerClient } = require("../Interfaces/DockerClient");
const { Terminal } = require("../Interfaces/Terminal");
const { ProjectMap } = require("../Models/Container");

class ContainerController {
	constructor() {
		this.tree = new TreeView("containers", {
			dataProvider: this
		});
		this.projects = undefined;
		
		this.register_commands();
		
		this.tree.onDidExpandElement((element) => {
			element.collapsed = TreeItemCollapsibleState.Expanded;
		  });
		
		this.tree.onDidCollapseElement((element) => {
			element.collapsed = TreeItemCollapsibleState.Collapsed;
		});
	}
	
	deactivate() {
		this.dispose();
	}
	
	reload() {
		this.tree.reload();
	}
	
	dispose() {
		this.tree.dispose();
	}
	
	async getChildren(element) {
		if (!element) {
			try {
				this.projects = await this.fetchContainers();
			} catch(e) {
				console.warn(e);
				return [];
			}
			return this.projects.valueArray();
		}
		else {
			return element.children;
		}
	}
	
	async fetchContainers() {
		let latest_projects = [];
		try {
			latest_projects = await DockerClient.get_containers();
		} catch(e) {
			console.warn(e);
		}
		this.projects && this.projects.forEach((old_project) => {
			let new_project = latest_projects.get(old_project.name);
			if (new_project && old_project.collapsed) {
				new_project.collapsed = old_project.collapsed;
			}
		});
		return latest_projects;
	}
	
	getParent(element) {
		// Requests the parent of an element, for use with the reveal() method
		return element.parent;
	}
	
	getTreeItem(element) {
		if (element.children.length > 0) {
			return new ProjectTreeItem(element);
		}
		else {
			return new ContainerTreeItem(element);
		}
	}
	
	// TODO: Compose and Containers are mixing. Separate?
	register_commands() {
		nova.commands.register("containers.reload", () => {
			this.reload();
		});
		
		nova.commands.register("containers.start", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			try {
				await DockerClient.start_container(selection);
			} catch(e) {
				let warning = "Unable to start container(s).";
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("containers.stop", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			try {
				await DockerClient.stop_container(selection);
			} catch(e) {
				let warning = "Unable to stop container(s).";
			}
		});
		
		nova.commands.register("containers.remove", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			try {
				await DockerClient.remove_container(selection);
			} catch(e) {
				let warning = "Unable to remove container(s).";
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("containers.open_terminal", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					await Terminal.open_terminal(selection.ID);
				} catch(e) {
					let warning = "Unable to open Terminal.";
					notify.Nudge(warning, e);
				}
			});
		});
		
		nova.commands.register("containers.tail_container", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					await Terminal.tail_container(selection.ID);
				} catch(e) {
					let warning = "Unable to tail Logs in Terminal.";
					notify.Nudge(warning, e);
				}
			});
		});
		
		nova.commands.register("container.inspect", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			selection.forEach(async (container_id) => {
				nova.workspace.openNewTextDocument({
					content: await DockerClient.inspect_container(container_id),
					syntax: "json"
				});
			});
		});
		
		nova.commands.register("compose.start", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					await DockerClient.start_compose(selection.config, selection.name);
				} catch(e) {
					let warning = "Unable to start compose.";
					notify.Nudge(warning, e);
				}	
			});
		});
		
		nova.commands.register("compose.stop", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					await DockerClient.stop_compose(selection.config, selection.name);
				} catch(e) {
					let warning = "Unable to stop compose.";
					notify.Nudge(warning, e);
				}	
			});
		});
		
		nova.commands.register("compose.down", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					await DockerClient.down_compose(selection.config, selection.name);
				} catch(e) {
					let warning = "Unable to down compose.";
					notify.Nudge(warning, e);
				}	
			});
		});
		
		nova.commands.register("compose.restart", async () => {
			let selection = this.tree.selection.forEach(async (selection) => {
				try {
					await DockerClient.restart_compose(selection.config, selection.name);
				} catch(e) {
					let warning = "Unable to restart compose.";
					notify.Nudge(warning, e);
				}	
			});
		});
		
		nova.config.onDidChange('docker.container.label', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.container.icons', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.container.desctext.names', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.context.desctext.id', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.container.desctext.runningfor', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.container.desctext.size', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.container.desctext.status', () => {
			this.reload();
		});
	}
}

module.exports = { ContainerController }
