const { DockerClient } = require("../interfaces/DockerClient");

class EventController {
	constructor() {
		this.register_commands();
		
		this.event_listener = DockerClient.event_listener.bind(this);
		this.start();
	}
	
	start() {
		if (this.event_process) {
			this.deactivate();
		}
		try {
			this.event_listener(this.event_multiplexer);
		} catch(e) {
			console.warn(e);
		}
	}
	
	deactivate() {
		if (this.event_process)
			this.event_process.terminate();
	}
	
	reload() {
		this.start();
	}
	
	dispose() {
		this.deactivate();
	}
	
	event_multiplexer(event_json) {
		
		let event = JSON.parse(event_json);
		
		if (event.Type == "container") {
			nova.commands.invoke("containers.reload");
		}
		
		if (event.Type == "image") {
			nova.commands.invoke("images.reload");
		}
		
		if (event.Type == "network") {
			nova.commands.invoke("networks.reload");
		}
		
		if (event.Type == "volume") {
			nova.commands.invoke("volumes.reload");
		}
		
	}
	
	register_commands() {
		nova.commands.register("events.reload", async () => {
			console.log("Reloading Event Listener");
			this.reload();
		});
	}

}

module.exports = { EventController }
