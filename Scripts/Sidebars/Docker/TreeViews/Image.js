const notify = require("../../../Utilities/Notify");
const { DockerClient } = require("../interfaces/DockerClient");
const { ImageRepoTreeItem } = require("../TreeItems/ImageRepoTreeItem");
const { ImageTreeItem } = require("../TreeItems/ImageTreeItem");

class ImageController {
	constructor() {
		this.tree = new TreeView("images", {
			dataProvider: this
		});
		this.images = undefined;
		
		this.register_commands();
		
		this.tree.onDidExpandElement((element) => {
			element.collapsed = TreeItemCollapsibleState.Expanded;
		  });
		
		this.tree.onDidCollapseElement((element) => {
			element.collapsed = TreeItemCollapsibleState.Collapsed;
		});
	}
	
	deactivate() {
		this.dispose();
	}
	
	reload() {
		this.tree.reload();
	}
	
	dispose() {
		this.tree.dispose();
	}
	
	async getChildren(element) {
		if (!element) {
			try {
				this.images = await this.fetchImages();
			} catch(e) {
				console.warn(e);
				return [];
			}
			return this.images.valueArray();
		}
		else {
			return element.children;
		}
	}
	
	async fetchImages() {
		var latest_images = [];
		try {
			latest_images = await DockerClient.get_images(ImageController.dangling);
		} catch(e) {
			console.warn(e);
			return latest_images;
		}
		this.images && this.images.forEach((old_image) => {
			let new_image = latest_images.get(old_image.name);
			if (new_image && old_image.collapsed) {
				new_image.collapsed = old_image.collapsed;
			}
		});
		return latest_images;
	}
	
	getParent(element) {
		// Requests the parent of an element, for use with the reveal() method
		return element.parent;
	}
	
	getTreeItem(element) {
		// Converts an element into its display (TreeItem) representation
		if (element.children.length > 0) {
			return new ImageRepoTreeItem(element);
		}
		return new ImageTreeItem(element);
	}
	
	static get dangling() {
		try {
			return nova.config.get("docker.sidebar.images.dangling");
		} catch(e) {
			return false;
		}
	}
	
	static set dangling(dangling_bool) {
		try {
			nova.config.set(
				"docker.sidebar.images.dangling",
				!ImageController.dangling
			);
		} catch(e) {
			return false;
		}
	}
	
	register_commands() {
		
		nova.config.onDidChange('docker.sidebar.images.dangling', () => {
			this.reload();
		});
		
		nova.commands.register('images.toggle.dangling', () => {
			ImageController.dangling = !ImageController.dangling;
		});
		
		nova.commands.register("images.reload", () => {
			this.reload();
		});
		
		nova.commands.register("images.run", async () => {
			this.tree.selection.forEach(async (selection) => {
				try {
					// We use image : tag for run commands,
					// otherwise the image name will be a hash
					let res = await DockerClient.run_image(
						selection.Repository,
						selection.Tag
					);
					if (res == undefined) {
						this.reload();
					}
				} catch(e) {
					let warning = "Unable to start image(s).";
					notify.Nudge(warning, e);
				}
			});
		});
		
		nova.commands.register("images.remove", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			try {
				await DockerClient.remove_image(selection);
			} catch(e) {
				let warning = "Unable to remove image(s)."
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("images.inspect", async () => {
			let selection = this.tree.selection.map(x => x.ID);
			
			selection.forEach(async (image_id) => {
				nova.workspace.openNewTextDocument({
					content: await DockerClient.inspect_image(image_id),
					syntax: "json"
				});
			});
		});
	}
}

module.exports = { ImageController }
