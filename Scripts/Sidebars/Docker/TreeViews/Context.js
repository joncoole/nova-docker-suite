const notify = require("../../../Utilities/Notify");
const { ContextTreeItem } = require("../TreeItems/ContextTreeItem");
const { DockerClient } = require("../interfaces/DockerClient");
const { Context } = require("../models/Context");

class ContextController {
	constructor() {
		this.tree = new TreeView("contexts", {
			dataProvider: this
		});
		
		this.register_commands();
		this.register_listeners();
	}
	
	deactivate() {
		this.dispose();
	}
	
	reload() {
		this.tree.reload();
	}
	
	dispose() {
		this.tree.dispose();
	}
	
	async getChildren(element) {
		if (!element) {
			let contexts = [];
			try {
				return await DockerClient.get_contexts();
			} catch(e) {
				return contexts;
			}
		}
		else {
			return element.children;
		}
	}
	
	getParent(element) {
		return element.parent;
	}
	
	getTreeItem(element) {
		return new ContextTreeItem(element);
	}
	
	register_commands() {
		nova.commands.register("contexts.use", async () => {
			let selection = this.tree.selection[0];
			
			let res = await DockerClient.use_context(selection.name);
			if (res == undefined) {
				nova.commands.invoke("docker_suite.sidebar.reload");
			}
		});
		
		nova.commands.register("contexts.inspect", async () => {
			let selection = this.tree.selection.map(x => x.Name);
			
			selection.forEach(async (context_id) => {
				nova.workspace.openNewTextDocument({
					content: await DockerClient.inspect_context(context_id),
					syntax: "json"
				});
			});
		});
		
		nova.commands.register("contexts.remove", async () => {
			let selection = this.tree.selection[0];
			
			try {
				let res = await DockerClient.remove_context(selection.name);
				if (res == undefined) {
					this.reload();
				}
			} catch(e) {
				let warning = "Unable to remove context.";
				notify.Nudge(warning, e);
			}
		});
		
		nova.commands.register("contexts.reload", async () => {
			console.log("Reloading Contexts");
			this.reload();
			nova.commands.invoke("events.reload");
		});
	}
	
	register_listeners() {
		nova.config.onDidChange('docker.context.label', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.context.icons', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.context.desctext.desc', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.context.desctext.endp', () => {
			this.reload();
		});
		
		nova.config.onDidChange('docker.context.desctext.name', () => {
			this.reload();
		});
	}
}

module.exports = { ContextController }
