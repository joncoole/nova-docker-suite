class Host {
  constructor() {
	if (Host._instance) {
	  return HostEnv._instance
	}
	Host._instance = this;
	this._arch = this.get_host_architecture();
  }
  
  get arch() {
	return this._arch;
  }
  
  async get_host_architecture() {
	  console.log("Checking host architecture.");
	  return new Promise((resolve, reject) => {
		  var response = "";
		  var uname = new Process("/usr/bin/env", {
			  args: ["uname", "-p"]
		  })
		  uname.onStdout((line) => {
			  response += line;
		  })
		  uname.onDidExit((return_code) => {
			  if (return_code != 0) {
				  reject(err);
			  }
			  resolve(response.trim());
		  })
		  uname.start();
	  });
  };
  
}

let host = new Host();

module.exports = { host, Host };