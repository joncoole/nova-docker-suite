const { notify_latest } = require("./Notify");

exports.VersionCheck = class VersionCheck {
	constructor() {
		this.local_version_key = `${nova.extension.identifier}.extension.version`;
		this.previous = nova.config.get(this.local_version_key, "string");
		this.current = nova.extension.version;
	}
	
	did_change() {
		if (this.previous == null || this.previous != this.current) {
			return true;
		}
		return false;
	}
	
	update() {
		if (this.did_change()) {
			nova.config.set(this.local_version_key, nova.extension.version)
			this.notify();
		}
	}
	
	notify() {
		notify_latest();
	}
}