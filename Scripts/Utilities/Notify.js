exports.Nudge = function(title, body, id = nova.crypto.randomUUID()) {
	title = title.toString();
	body = body.toString();
	let request = new NotificationRequest(id);
	request.title = nova.localize(title);
	request.body = nova.localize(body);
	nova.notifications.add(request);
}

exports.notify_latest = function () {
	let request = new NotificationRequest('docker-latest');
	request.title = `${nova.extension.name} Updated`;
	request.body = `${nova.extension.name} has been updated to: ${nova.extension.version}`;
	request.actions = [nova.localize("Release Notes"), nova.localize("Ok")]

	let promise = nova.notifications.add(request);
	promise.then(reply => {
		if (reply.actionIdx == 0) {
			nova.extension.openChangelog();
		}
	}, error => {
		console.error(error);
	});
}

exports.ConfigNudge = function(title, body, id = nova.crypto.randomUUID()) {
	title = title.toString();
	body = body.toString();
	let notice_request = new NotificationRequest(id);
	notice_request.title = nova.localize(title);
	notice_request.body = nova.localize(body);
	
	let actions = new Map(Object.entries({
		"Help": () => {
			nova.extension.openHelp();
		},
		"Reload": () => {
			nova.commands.invoke("docker_suite.sidebar.reload");	
		},
		"OK": () => {}
	}));
	notice_request.actions = [...actions.keys()];
	nova.notifications.add(notice_request).then(reply => {
		actions.get(
			notice_request.actions[reply.actionIdx]
		)()
	});
}

exports.HelpNudge = function(title, body, id = nova.crypto.randomUUID()) {
	title = title.toString();
	body = body.toString();
	console.log(id);
	let notice_request = new NotificationRequest(id);
	notice_request.title = nova.localize(title);
	notice_request.body = nova.localize(body);
	
	let actions = new Map(Object.entries({
		"Help": () => {
			nova.extension.openHelp();
		},
		"Settings": () => {
			nova.openConfig();	
		},
		"OK": () => {}
	}));
	notice_request.actions = [...actions.keys()];
	nova.notifications.add(notice_request).then(reply => {
		actions.get(
			notice_request.actions[reply.actionIdx]
		)()
	});
}