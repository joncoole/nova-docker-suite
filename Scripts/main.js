const { DockerSidebar } = require("./Sidebars/Docker/DockerSidebar");
const { RepoLanguageClient } = require('./LanguageClients/RepoLanguageClient');
const { VersionCheck } = require("./Utilities/VersionCheck");

try {
    version_check = new VersionCheck();
    version_check.update();
} catch (err) {
    if (nova.inDevMode()) {
        console.error(err);
    }
}

let dockerfile_langserver = null;
let compose_langserver = null;
let docker_sidebar = null;

exports.activate = async function() {
    // Do work when the extension is activated
    dockerfile_langserver = new RepoLanguageClient({
        repo_url: 'https://gitlab.com/joncoole/nova-lsp-binary/raw/master/',
        lsp_name: 'docker-langserver-macos',
        service_vector: 'dockerfile.lsp.vector',
        user_path_conf: 'docker.language-server-path',
        name: 'Dockerfile LC',
        syntax: ['dockerfile']
    });
    compose_langserver = new RepoLanguageClient({
        repo_url: 'https://gitlab.com/joncoole/nova-lsp-binary/raw/master/',
        lsp_name: 'docker-compose-langserver-macos',
        service_vector: 'compose.lsp.vector',
        user_path_conf: 'compose.language-server-path',
        name: 'Compose LS',
        syntax: [
            'compose',
            'yaml'
        ]
    });
    docker_sidebar = new DockerSidebar();
    
    dockerfile_langserver.start();
    compose_langserver.start();
}

exports.deactivate = function() {
    // Clean up state before the extension is deactivated
    if (dockerfile_langserver) {
        dockerfile_langserver.stop();
        dockerfile_langserver = null;
    }
    
    if (compose_langserver) {
        compose_langserver.stop();
        compose_langserver = null;
    }
    
    if (docker_sidebar) {
        docker_sidebar.stop();
        docker_sidebar = null;
    }
}

nova.commands.register("docker_suite.command.reload", () => {
    exports.deactivate();
    exports.activate();
});
