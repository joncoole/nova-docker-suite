# Docker Suite 
Docker Sidebar for managing containers, contexts and images.

Dockerfile, Compose (+Bash) syntax and Language Server support.

More features to come...

## Language Support

* Enhanced syntax highlighting using tree-sitter:
	- Dockerfile
	- Compose
	- Bash

* Dockerfile & Compose Language Server support including:
	- Autocompletion
	- Documentation
	- Validation
	- Hover
	- Linting
	
* Stand-alone Docker & Compose LS Binary (optional)
	- No dependencies.
	- No manual setup.
	- Enable if you want it, or provide your own.

## Docker Sidebar

* Docker Client Sidebar
	- View Contexts, Containers, Projects and Images
	- Start, Stop, Remove etc...
	- More features to come!

## Future roadmap
- Sidebar for managing Volumes.
- Container File explorer.
