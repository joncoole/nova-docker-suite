# Guides and Troubleshooting

## Notice regarding Compose Language Service
- Nova does not handle multiple LS for a single syntax
	- As such, Compose LS activates on YAML & Compose syntax
- Nova *ALWAYS* prioritizes built-in syntax, namely built-in YAML
	- As such, Nova does not allow me to auto-detect Compose syntax
**Due to these API limitations**
- Nova does not allow me to detect Compose syntax over their built-in YAML
- Nova will override the Compose LS with *ANY* YAML LS
	- If you are using a YAML LS, manually select Compose syntax to bypass


## Dockerfile & Compose Language Server Installation

**NOTE** Compose LS support is currently experimental

In order to use all the benefits of LSP, you must select an LSP provider.

Fortunately, `Docker Suite` provides a stand-alone option that makes this automatic!

For those who wish to provide their own, `Docker Suite` supports that as well!

### Stand-alone LSP (Experimental, but recommended)
**Experimental**
1. Under Extensions, select `Docker Suite`, then Settings
2. Navigate to `Language Services`, then to the desired LS (Dockerfile or Compose)
3. Under `Select <language> LSP`, choose `Portable Binary`
	- This will download and configure a stand-alone binary language server.
	- No extra steps. After downloading, thats it!

### User provided LSP

**If you prefer to provide your own LSP, use the following steps.**
1. Install [Brew](https://brew.sh)
2. Install [Node.js](https://formulae.brew.sh/formula/node)
3. Install the desired LSP:
	- For Dockerfile LSP: [Docker Language Server](https://www.npmjs.com/package/dockerfile-language-server-nodejs)
	- For Compose LSP: [Docker Compose Language Service](https://www.npmjs.com/package/@microsoft/compose-language-service)
4. Copy/Paste the full LSP executable path in the `Docker Suite` preferences.
	- `which docker-langserver`
	- `which docker-compose-langserver`

## Docker Sidebar Dependency

Currently the Docker Sidebar requires the Docker CLI to be installed.

If you don't already, the following steps will show you how:
1. Install [Brew](https://brew.sh)
2. Install [Docker](https://formulae.brew.sh/formula/docker#default)
